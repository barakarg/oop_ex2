package oop.ex2.commands;

import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.CommandType;
import oop.ex2.main.ProgramConstants.RegexPatterns;

/**
 * this class represents a command. it is extended by specific command types.
 *
 */
public class Command {

    private CommandType type;
    private String commandString;
    protected RegexPatterns rp;

    public Command(String commandString) throws Exception {
        this.commandString = commandString;
        // init regex patterns
        rp = new ProgramConstants.RegexPatterns();
        detectCommandType();
    }

    private void detectCommandType() throws Exception {

        // check value against various types
        if ((rp.CONDITION_COMMAND.matcher(commandString)).matches()) {
            type = CommandType.CONDITION;
            return;
        }
        if ((rp.METHOD_DECLARATION.matcher(commandString)).matches()) {
            type = CommandType.METHOD_DECLARATION;
            return;
        }
        if ((rp.RETURN_COMMAND.matcher(commandString)).matches()) {
            type = CommandType.RETURN;
            return;
        }
        if ((rp.VARIABLE_DECLARATION.matcher(commandString)).matches()) {
            type = CommandType.VARIABLE_DECLARATION;
            return;
        }
        if ((rp.METHOD_CALL.matcher(commandString)).matches()) {
            type = CommandType.CALL;
            return;
        }
        if ((rp.METHOD_ARGUMENTS.matcher(commandString)).matches()) {
            type = CommandType.VARIABLE_DECLARATION;
            return;
        }
        if ((rp.ASSIGNMENT_COMMAND.matcher(commandString)).matches()) {
            type = CommandType.ASSIGNMENT;
            return;
        }
        throw new Exception("undefined command type.");
        
    }

    public CommandType getType() {
        return type;
    }

    public String getCommandString() {
        return commandString;
    }

}

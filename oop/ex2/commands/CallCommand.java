package oop.ex2.commands;

import java.util.ArrayList;
import java.util.regex.Matcher;

import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.RegexPatterns;

/**
 * this class represents a call to a method command.
 *
 */
public class CallCommand extends Command {

    private String name;
    private ArrayList<String> arguments;

    public CallCommand(String commandString) throws Exception {
        super(commandString);
        arguments = new ArrayList<String>();
        // parse line
        Matcher callMatcher = rp.METHOD_CALL.matcher(commandString);
        if (callMatcher.matches()) {
            name = callMatcher.group(rp.METHOD_CALL_GROUP_NAME);
            // split arguments
            String argumentsStr = callMatcher.group(rp.METHOD_CALL_GROUP_ARGUMENTS_STRING);
            if (argumentsStr.length()>0) {
                for (String argStr : splitToArguments(argumentsStr)) {
                    arguments.add(argStr);
                }
            }
        }
    }

    public static ArrayList<String> splitToArguments(String argumentsString) throws Exception {
        ArrayList<String> arguments = new ArrayList<String>();
        int bracketsCount = 0;
        String current = "";
        for (char c : argumentsString.toCharArray()) {
            if (c == '(') {
                bracketsCount++;
            }
            else if (c == ')') {
                bracketsCount--;
                if (bracketsCount<0) {
                    throw new Exception("invalid order of brackets in expression.");
                }
            }
            else if (c == ',') {
                if (bracketsCount == 0) {
                    arguments.add(current);
                    current = "";
                    continue;
                }
            }
            current = current.concat(Character.toString(c));
        }
        arguments.add(current);

        return arguments;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getArguments() {
        return arguments;
    }
}

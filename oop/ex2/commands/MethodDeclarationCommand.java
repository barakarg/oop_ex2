package oop.ex2.commands;

import java.util.ArrayList;
import java.util.regex.Matcher;

import oop.ex2.main.ProgramConstants.VarType;

public class MethodDeclarationCommand extends Command {

    private String name;
    private VarType varType;
    private ArrayList<VariableDeclarationCommand> gets;
    private String code;

    public MethodDeclarationCommand(String commandString) throws Exception {
        super(commandString);
        gets = new ArrayList<VariableDeclarationCommand>();
        // parse line
        Matcher declarationMatcher = rp.METHOD_DECLARATION.matcher(commandString);
        if (declarationMatcher.matches()) {
            varType = VarType.getByValue(declarationMatcher.group(
                    rp.METHOD_DECLARATION_GROUP_VAR_TYPE));
            code = declarationMatcher.group(rp.METHOD_DECLARATION_GROUP_CODE);
            name = declarationMatcher.group(rp.METHOD_DECLARATION_GROUP_NAME);
            // split arguments
            String argumentsStr =
                    declarationMatcher.group(rp.METHOD_DECLARATION_GROUP_ARGUMENTS_STRING);
            if (argumentsStr.length()>0) {
                for (String argStr : splitToArguments(argumentsStr)) {
                    gets.add(new VariableDeclarationCommand(argStr, true));
                }
            }
        }
    }

    private ArrayList<String> splitToArguments(String argumentsStr) throws Exception {
        return CallCommand.splitToArguments(argumentsStr);
    }

    public MethodDeclarationCommand(Command var) throws Exception {
        this(var.getCommandString());
    }

    public ArrayList<VariableDeclarationCommand> getGets() {
        return gets;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public VarType getVarType() {
        return varType;
    }
}

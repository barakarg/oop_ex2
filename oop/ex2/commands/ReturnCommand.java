package oop.ex2.commands;

import java.util.regex.Matcher;

public class ReturnCommand extends Command {

    private String value;

    public ReturnCommand(String commandString) throws Exception {
        super(commandString);
        // parse line
        Matcher matcher = rp.RETURN_COMMAND.matcher(commandString);
        if (matcher.matches()) {
            value = matcher.group(rp.RETURN_COMMAND_GROUP_VALUE);
        }
    }

    public String getValue() {
        return value;
    }
}

package oop.ex2.commands;

import static oop.ex2.main.ProgramConstants.BRACKET_CLOSE_CHAR;
import static oop.ex2.main.ProgramConstants.BRACKET_OPEN_CHAR;
import static oop.ex2.main.ProgramConstants.NEWLINE_CHAR;

import java.util.ArrayList;

import oop.ex2.main.ProgramConstants.CommandType;

/**
 * This class represents block of commands.
 *
 */
public class CommandsBlock {

    // variable that stores the commands list
    private ArrayList<Command> commands;

    /**
     * Constructs list of commands from a given code block string.
     * @param code code string
     * @throws Exception
     */
    public CommandsBlock(String code) throws Exception {
        // init commands array
        commands = new ArrayList<Command>();

        // scan the code and divide to commands
        String commandString = "";
        int bracketCount = 0;
        for (String line : code.split(NEWLINE_CHAR)) {
            if (!line.isEmpty()) {
                // add line to current command
                if (commandString.length()>0) { // if not first line of command.
                    commandString = commandString.concat(NEWLINE_CHAR);
                }
                commandString = commandString.concat(line);

                // check for brackets
                if (line.endsWith(BRACKET_OPEN_CHAR)) {
                    bracketCount++;
                }
                else if (line.endsWith(BRACKET_CLOSE_CHAR)) {
                    bracketCount--;
                    // check for illegal brackets structure
                    if (bracketCount<0) {
                        throw new Exception("illegal brackets structure.");
                    }
                }

                // if current command ends - add it to array and init new one
                if (bracketCount == 0) {
                    if (!commandString.equals(NEWLINE_CHAR)) {
                        // empty lines are to be ignored
                        commands.add(new Command(commandString));
                    }
                    commandString = "";
                }
            }
        }
    }

    public ArrayList<Command> getCommands() {
        return commands;
    }

    public ArrayList<Command> getCommands(CommandType filter) {
        ArrayList<Command> tmpCmds = new ArrayList<Command>();
        for (Command cmd : commands) {
            if (cmd.getType() == filter) {
                tmpCmds.add(cmd);
            }
        }
        return tmpCmds;
    }

//    public Method[] getMethods() {
//        // call getCommands with METHOD_DECLATATION type as filter
//        return getCommands(CommandType.METHOD_DECLARATION);
//    }

}

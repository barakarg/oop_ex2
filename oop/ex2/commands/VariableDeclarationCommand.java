package oop.ex2.commands;

import java.util.regex.Matcher;

import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;

public class VariableDeclarationCommand extends Command {

    private VarType varType;
    private boolean isFinal;
    private String name;
    private String value;

    public VariableDeclarationCommand(String commandString) throws Exception {
        super(commandString);
        // parse line
        Matcher declarationMatcher = rp.VARIABLE_DECLARATION.matcher(commandString);
        if (declarationMatcher.matches()) {
            varType = VarType.getByValue(declarationMatcher.group(
                    rp.VARIABLE_DECLARATION_GROUP_VAR_TYPE));
            isFinal = (declarationMatcher.group(
                    rp.VARIABLE_DECLARATION_GROUP_IS_FINAL) != null);
            value = declarationMatcher.group(rp.VARIABLE_DECLARATION_GROUP_VALUE);
            name = declarationMatcher.group(rp.VARIABLE_DECLARATION_GROUP_NAME);
        }
    }

    public VariableDeclarationCommand(String commandString, boolean asMethodArgument)
            throws Exception {
        super(commandString);
        // parse line
        Matcher declarationMatcher = rp.METHOD_ARGUMENTS.matcher(commandString);
        if (declarationMatcher.matches()) {
            varType = VarType.getByValue(declarationMatcher.group(
                    rp.METHOD_ARGUMENTS_GROUP_VAR_TYPE));
            isFinal = (declarationMatcher.group(
                    rp.METHOD_ARGUMENTS_GROUP_IS_FINAL) != null);
            name = declarationMatcher.group(rp.METHOD_ARGUMENTS_GROUP_NAME);
            // set default value so the variable will be count as initiated.
            value = varType.getExampleValue();
        }
    }

    public VariableDeclarationCommand(Command var) throws Exception {
        this(var.getCommandString());
    }

    public boolean getIsFinal() {
        return isFinal;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public VarType getVarType() {
        return varType;
    }
}

package oop.ex2.commands;

import java.util.HashMap;

public interface CommandData {

    // implementations have to have constructor:
    // XxxCommandData(String commandString)

    public HashMap<String, String> getDataArray();

}

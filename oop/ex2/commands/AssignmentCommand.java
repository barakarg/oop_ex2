package oop.ex2.commands;

import java.util.regex.Matcher;

import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;


public class AssignmentCommand extends Command {

    private String name;
    private String argument;
    private VarType varType;
    private boolean isDeclaration;
    
    public AssignmentCommand(String commandString) throws Exception {
        super(commandString);
        // parse line
        Matcher matcher = rp.ASSIGNMENT_COMMAND.matcher(commandString);
        if (matcher.matches()) {
            name = matcher.group(rp.ASSIGNMENT_COMMAND_GROUP_NAME);
            argument = matcher.group(rp.ASSIGNMENT_COMMAND_GROUP_VALUE);
        }
    }

    public String getArgument() {
        return argument;
    }

    public void setVarType(VarType argumentType) {
        varType = argumentType;
        
    }

    public VarType getVarType() {
        return varType;
    }

    public boolean isDeclaration() {
        return isDeclaration;
    }

    public String getName() {
        return name;
    }
}

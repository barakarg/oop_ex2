package oop.ex2.commands;

import java.util.regex.Matcher;

public class ConditionCommand extends Command {

    private String condition;
    private String code;

    public ConditionCommand(String commandString) throws Exception {
        super(commandString);
        // parse line
        Matcher matcher = rp.CONDITION_COMMAND.matcher(commandString);
        if (matcher.matches()) {
            condition = matcher.group(rp.CONDITION_COMMAND_GROUP_CONDITION);
            code = matcher.group(rp.CONDITION_COMMAND_GROUP_CODE);
        }
    }

//    public ConditionType getConditionType() {
//        return null;
//    }

    public String getCondition() {
        return condition;
    }

    public String getCode() {
        return code;
    }
}

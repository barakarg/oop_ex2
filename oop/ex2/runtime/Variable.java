package oop.ex2.runtime;

import oop.ex2.commands.AssignmentCommand;
import oop.ex2.commands.VariableDeclarationCommand;
import oop.ex2.main.ProgramConstants.VarType;

/**
 * This class represents a variable
 *
 */
public class Variable {

    private String name;
    private VarType type;
    private String value;
    private boolean isFinal;

    public Variable(VariableDeclarationCommand command) {
        name = command.getName();
        type = command.getVarType();
        value = command.getValue();
        isFinal = command.getIsFinal();
    }

    public Variable(AssignmentCommand command) {
        name = command.getName();
        type = command.getVarType();
        value = command.getArgument();
    }

    public String getName() {
        return name;
    }

    public VarType getType() {
        return type;
    }

    public boolean getIsFinal() {
        return isFinal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String newValue) {
        value = newValue;
    }
}

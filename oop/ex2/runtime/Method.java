package oop.ex2.runtime;

import java.util.ArrayList;

import oop.ex2.commands.Command;
import oop.ex2.commands.MethodDeclarationCommand;
import oop.ex2.commands.VariableDeclarationCommand;
import oop.ex2.main.ProgramConstants.VarType;


/**
 * This class represents a method.
 *
 */
public class Method {
    ArrayList<Variable> gets;
    String code;
    private String name;
    private VarType type;
    
    public Method(MethodDeclarationCommand command) {
        gets = new ArrayList<Variable>();
        if (command.getGets() != null) {
            for (VariableDeclarationCommand getVariable : command.getGets()) {
                gets.add(new Variable(getVariable));
            }
        }
        name = command.getName();
        type = command.getVarType();
        code = command.getCode();
    }

    public String getName() {
        return name;
    }

    public VarType getType() {
        return type;
    }

    public String getCode() {
        return code.toString(); // TODO GOOD??
    }

    public ArrayList<Variable> getGets() {
        return gets;
    }

}

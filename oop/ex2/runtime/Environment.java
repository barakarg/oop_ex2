package oop.ex2.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;

import oop.ex2.commands.CallCommand;
import oop.ex2.commands.Command;
import oop.ex2.commands.CommandsBlock;
import oop.ex2.commands.MethodDeclarationCommand;
import oop.ex2.commands.VariableDeclarationCommand;
import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.CommandType;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.validations.CallValidation;

/**
 * this class represents an environment of a method / code block / global.
 *
 */
public class Environment {

    private Environment globalEnvironment;
    private boolean isGlobalEnvironment;

    private HashMap<String, Variable> variables;
    private HashMap<String, Method> methods;

    /**
     * initializes a (global) environment by a given commands block.
     * @param commandsBlock commands block
     * @throws Exception 
     */
    public Environment(CommandsBlock commandsBlock) throws Exception {
        // set this environment's global environment
        setGlobalSettings(null);

        // parse method and variable declarations in commands block and add
        // them to the environment
        for (Command var : commandsBlock.getCommands(CommandType.METHOD_DECLARATION)) {
            writeMethod(new Method(new MethodDeclarationCommand(var)));
        }
        for (Command var : commandsBlock.getCommands(CommandType.VARIABLE_DECLARATION)) {
            writeVariable(new Variable(new VariableDeclarationCommand(var)), true);
        }
        
        for (Command var : commandsBlock.getCommands()) {
            if (CommandType.VARIABLE_DECLARATION != var.getType() &&
                    CommandType.METHOD_DECLARATION != var.getType()){
                // illegal command in general scope
                throw new Exception("illegal command in general scope.");
            }
        }
    }

    /**
     * initializes an environment for a given method.
     * @param globalEnvironment existing global environment
     * @param method method
     * @throws Exception 
     */
    public Environment(Environment globalEnvironment, Method method)
            throws Exception {
        // call default constructor to set the global environment.
        setGlobalSettings(globalEnvironment);

        // assign all variables from method declaration to the environment.
        if (method.getGets() != null) {
            for (Variable get : method.getGets()) {
                writeVariable(get, true);
            }
        }
    }

    /**
     * initializes a duplicated environment of a given one.
     * @param source environment to duplicate
     */
    public Environment(Environment source) {
        this.isGlobalEnvironment = source.isGlobalEnvironment;
        this.globalEnvironment = (isGlobalEnvironment)? null :
            source.globalEnvironment.clone();
        this.variables = (HashMap<String, Variable>) source.variables.clone();
        this.methods = (HashMap<String, Method>) source.methods.clone();
    }

    /**
     * sets a global environment according to a given one.
     * @param globalEnvironment
     */
    private void setGlobalSettings(Environment globalEnvironment) {
        // set global environment.
        this.globalEnvironment = globalEnvironment;
        // define environment as global/local
        isGlobalEnvironment = (this.globalEnvironment == null) ? true : false;
        // init arrays
        methods = new HashMap<String, Method>();
        variables = new HashMap<String, Variable>();
    }

    public VarType readVariable(String name) throws Exception {
        Variable var = getVariable(name);
        if (var == null) {
            throw new Exception("non existing variable requested.");
        }
        if (var.getValue() == null) {
            throw new Exception("uninitialized variable's value requested.");
        }
        return var.getType();
    }

    private Variable getVariable(String name) {
        Variable var = variables.get(name);
        // if no such variable, look for it in gloabl environment
        if (var == null && !isGlobalEnvironment) {
            var = globalEnvironment.variables.get(name);
        }
        return var;
    }

    public boolean isVariableInitialized(String name) {
        Variable var = getVariable(name);
        return (var.getValue() != null);
    }

    public void writeVariable(Variable newVar, boolean isDeclaration)
            throws Exception {
        // check if there is already a variable with that name
        boolean isLocal = true;
        Variable var = variables.get(newVar.getName());
        if (var == null) {
            var = getVariable(newVar.getName());
            if (var != null) {
                isLocal = false;
            }
        }

        // check if the operation is assignment or declaration
        if (isDeclaration) {
            if (var != null && isLocal) {
                throw new Exception("cannot declare two variables with the same name.");
            }
            // else - create local variable

            // check if constant declaration without value
            if (isDeclaration && newVar.getIsFinal() && newVar.getValue() == null) {
                throw new Exception("trying to declare constant with no value.");
            }
            // check if the value matches the variable type
            if (newVar.getValue() != null) {
                if (!checkVarTypeMatch(newVar.getType(), detectValueType(newVar.getValue()))) {
                    throw new Exception("new value data type doesnt match variable type.");
                }
            }
            // creating new variable
            variables.put(newVar.getName(), newVar);
        }
        else {
            // assignment command
            if (var != null) {
                if (var.getIsFinal()) {
                    throw new Exception("cannot overwrite final variable.");
                }
                // else - assign the new value

                // check if the value matches the variable type
                if (!checkVarTypeMatch(var.getType(), newVar.getType())) {
                    throw new Exception("new value data type doesnt match variable type.");
                }
                // check that the new variable is initialized
                if (newVar.getValue() == null) {
                    throw new Exception("cannot read uninitialized variable.");
                }
                // update variable's data
                var.setValue(newVar.getValue());
            }
            else if (var == null) {
                throw new Exception("trying to assign value to non existing variable.");
            }
        }

    }

    public boolean checkVarTypeMatch(VarType type, VarType value) {
        // check variable type match between variable and a new value
        if (type == value) {
            // both are the same type
            return true;
        }
        // exceptions
        if (type == VarType.BOOLEAN && (value == VarType.INT || value == VarType.DOUBLE)) {
            return true;
        }
        if (type == VarType.DOUBLE && value == VarType.INT) {
            return true;
        }
        // else - doesnt match
        return false;
    }

    private void writeMethod(Method method) throws Exception {
        if (!isGlobalEnvironment) {
            throw new Exception("cannot declare methods inside method.");
        }
        if (methods.containsKey(method.getName())) {
            throw new Exception("cannot overwrite a method.");
        }
        methods.put(method.getName(), method);

    }

    public ArrayList<Method> getMethods() {
        return new ArrayList<Method>(
                (isGlobalEnvironment)? methods.values() :
                    globalEnvironment.methods.values());
    }

    public Environment clone() {
        return new Environment(this);
    }

    public Method getMethod(String name) throws Exception {
        String strippedName;
        // isolate method name of the call to the function
        // init regex patterns
        RegexPatterns rp = new ProgramConstants.RegexPatterns();

        Matcher methodNameMatcher = rp.METHOD_CALL.matcher(name);
        if (methodNameMatcher.matches()) {
            strippedName = methodNameMatcher.group(rp.METHOD_CALL_GROUP_NAME);
        }
        else {
            strippedName = name;
        }
        Method method = (isGlobalEnvironment)? methods.get(strippedName) :
            globalEnvironment.methods.get(strippedName);
        if (method == null) {
            throw new Exception("error - calling undefined method.");
        }
        return method;
    }

    public VarType detectValueType(String argument) throws Exception {
        // check if null
        if (argument == null) {
            return null;
        }
        
        // init regex patterns
        RegexPatterns rp = new ProgramConstants.RegexPatterns();

        // check value against various types
        if ((rp.VALUE_BOOLEAN.matcher(argument)).matches()) {
            return VarType.BOOLEAN;
        }
        if ((rp.VALUE_INT.matcher(argument)).matches()) {
            return VarType.INT;
        }
        if ((rp.VALUE_DOUBLE.matcher(argument)).matches()) {
            return VarType.DOUBLE;
        }
        if ((rp.VALUE_CHAR.matcher(argument)).matches()) {
            return VarType.CHAR;
        }
        if ((rp.VALUE_STRING.matcher(argument)).matches()) {
            return VarType.STRING;
        }
        // check for method call / variable
        Matcher argumentCall = rp.ARGUMENT_CALL.matcher(argument);
        if (argumentCall.matches()) {
            // validate inner method call statement
            CallValidation.validate(new CallCommand(argumentCall.group()), this);
            // return method
            return getMethod(argumentCall.group()).getType();
        }
        Matcher argumentVariable = rp.ARGUMENT_VARIABLE.matcher(argument);
        if (argumentVariable.matches()) {
            VarType varType = readVariable(argumentVariable.group());
            if (isVariableInitialized(argumentVariable.group())) {
                return varType;
            }
        }
        // no matching data type found - error.
        throw new Exception("undefined data type.");
    }
}

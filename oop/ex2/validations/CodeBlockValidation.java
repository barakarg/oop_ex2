package oop.ex2.validations;

import oop.ex2.commands.AssignmentCommand;
import oop.ex2.commands.CallCommand;
import oop.ex2.commands.Command;
import oop.ex2.commands.CommandsBlock;
import oop.ex2.commands.ConditionCommand;
import oop.ex2.commands.ReturnCommand;
import oop.ex2.commands.VariableDeclarationCommand;
import oop.ex2.main.ProgramConstants.CommandType;
import oop.ex2.runtime.Environment;
import oop.ex2.runtime.Method;
import oop.ex2.runtime.Variable;

/**
 * validates a block of code of a method or a part of it.
 *
 */
public class CodeBlockValidation {

    public static void validate(Method method, String code, Environment environment)
            throws Exception {
        // parse commands from code
        CommandsBlock commands = new CommandsBlock(code);

        ConditionCommand conditionCmd;
        AssignmentCommand assignmentCmd;
        VariableDeclarationCommand declarationCmd;
        CallCommand callCmd;
        ReturnCommand returnCmd;

        // for each command
        for (Command command : commands.getCommands()) {
            // if the command is a condition
            if (command.getType() == CommandType.CONDITION) {
                // validate condition statement
                conditionCmd = new ConditionCommand(command.getCommandString());
                // validate code inside
                ConditionValidation.validate(conditionCmd.getCondition(), environment);
                validate(method, conditionCmd.getCode(), environment.clone());
            }
            else if (command.getType() == CommandType.ASSIGNMENT) {
                // validate assignment parameters
                assignmentCmd = new AssignmentCommand(command.getCommandString());
                AssignmentValidation.validate(assignmentCmd, environment);
            }
            else if (command.getType() == CommandType.VARIABLE_DECLARATION) {
                // validate declaration parameters
                declarationCmd = new VariableDeclarationCommand(command.getCommandString());
                environment.writeVariable(new Variable(declarationCmd), true);
                //AssignmentValidation.validate(declarationCmd, environment);
            }
            else if (command.getType() == CommandType.CALL) {
                // validate method call statement
                callCmd = new CallCommand(command.getCommandString());
                CallValidation.validate(callCmd, environment);
            }
            else if (command.getType() == CommandType.RETURN) {
                // validate return statement
                returnCmd = new ReturnCommand(command.getCommandString());
                ReturnValidation.validate(method, returnCmd, environment);
            }
            else {
                // illegal command type inside a method
                throw new Exception("illegal command type inside method.");
            }
        }
    }
}

package oop.ex2.validations;

import oop.ex2.commands.CallCommand;
import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.runtime.Environment;

public class ArgumentValidation {

    public static VarType validate(String argument, Environment environment)
            throws Exception {
        VarType argumentType;
        // init regex patterns
        RegexPatterns rp = new ProgramConstants.RegexPatterns();

        // check if argument is a method call
        if ((rp.ARGUMENT_CALL.matcher(argument)).matches()) {
            // validate correct call
            argumentType = CallValidation.validate(
                    new CallCommand(argument), environment);
        }
        // check if argument is a simple value
        else if ((rp.ARGUMENT_VALUE.matcher(argument)).matches()) {
            // get the variable type
            argumentType = environment.detectValueType(argument);
        }
        // check if argument is an initialized variable
        else if ((rp.ARGUMENT_VARIABLE.matcher(argument)).matches()) {
            // validate initialized variable
            argumentType = environment.readVariable(argument);
        }
        else {
            // illegal value in a method call
            throw new Exception("illegal value in a method call/condition.");
        }

        return argumentType;
    }

}

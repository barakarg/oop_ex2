package oop.ex2.validations;

import java.util.ArrayList;

import oop.ex2.commands.CallCommand;
import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.runtime.Environment;
import oop.ex2.runtime.Method;

public class CallValidation {

    public static VarType validate(CallCommand ccmd, Environment environment)
            throws Exception {
        // get called method details from environment
        Method method = environment.getMethod(ccmd.getName());

        // check arguments
        ArrayList<String> arguments = ccmd.getArguments();
        VarType argumentType;
        if (arguments != null) {
            for (int i=0; i<arguments.size(); i++) {
                // validate argument
                argumentType = ArgumentValidation.validate(arguments.get(i), environment);
                // compare argument type to expected one
                if (method.getGets().get(i).getType() != argumentType) {
                    throw new Exception("type mismatch in method call arguments.");
                }
            }
        }

        // validation succeeded, return method's call return type
        return method.getType();
    }
}

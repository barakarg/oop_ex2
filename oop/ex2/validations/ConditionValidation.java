package oop.ex2.validations;

import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.runtime.Environment;

public class ConditionValidation {

    public static void validate(String condition, Environment environment)
            throws Exception {
        // parse commands from code
        VarType type = ArgumentValidation.validate(condition, environment);
        if (type != VarType.BOOLEAN && type != VarType.INT && type != VarType.DOUBLE) {
            throw new Exception("illegal argument in condition statement.");
        }
    }

}

package oop.ex2.validations;

import oop.ex2.commands.ReturnCommand;
import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.runtime.Environment;
import oop.ex2.runtime.Method;

public class ReturnValidation {

    public static void validate(Method method, ReturnCommand rcmd,
            Environment environment) throws Exception {
        // validate matching return type
        if (!environment.checkVarTypeMatch(method.getType(),
                (rcmd.getValue() == null) ? VarType.VOID :
                    environment.detectValueType(rcmd.getValue()))) {
            throw new Exception("illegal return command.");
        }
    }
}

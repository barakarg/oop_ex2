package oop.ex2.validations;

import oop.ex2.commands.AssignmentCommand;
import oop.ex2.commands.CallCommand;
import oop.ex2.main.ProgramConstants;
import oop.ex2.main.ProgramConstants.CommandType;
import oop.ex2.main.ProgramConstants.RegexPatterns;
import oop.ex2.main.ProgramConstants.VarType;
import oop.ex2.runtime.Environment;
import oop.ex2.runtime.Variable;

public class AssignmentValidation {

    public static void validate(AssignmentCommand assignmentCmd,
            Environment environment) throws Exception {

        // check argument
        VarType argumentType = ArgumentValidation.validate(
                assignmentCmd.getArgument(), environment);

        // update the environment

        // update the argument's current variable type at runtime
        assignmentCmd.setVarType(argumentType);
        // assign the variable to the environment
        environment.writeVariable(new Variable(assignmentCmd),
                assignmentCmd.isDeclaration());
    }

}

package oop.ex2.main;

import java.util.regex.Pattern;

/**
 * This class contains program-wide used constants
 *
 */
public class ProgramConstants {

    public static int RETURN_CODE_CORRECT = 0;
    public static int RETURN_CODE_INCORRECT = 1;
    public static int RETURN_CODE_IOERROR = 2;

    public static String NEWLINE_CHAR = "\n";
    
    public static String BRACKET_OPEN_CHAR = "{";
    public static String BRACKET_CLOSE_CHAR = "}";

    // enums

    public enum VarType {
        INT("int","0"),
        DOUBLE("double","0.0"),
        STRING("String","\"A\""),
        BOOLEAN("boolean","true"),
        CHAR("char","'c'"),
        VOID("void",""),
        NULL("null","");
        
        private final String value;
        private final String exampleValue;
        
        VarType(String value, String exampleValue) {
            this.value = value;
            this.exampleValue = exampleValue;
        }

        public static VarType getByValue(String value) {
            if (value != null) {
                for (VarType vt : VarType.values()) {
                    if (vt.value.equals(value)) {
                        return vt;
                    }
                }
            }
            return null;
        }
        
        public String getExampleValue() {
            return exampleValue;
        }
    }

    public enum CommandType {
        ASSIGNMENT,
        VARIABLE_DECLARATION,
        METHOD_DECLARATION,
        CONDITION,
        CALL,
        RETURN;
      }

    public enum ConditionType {
        IF,
        WHILE;
    }

    // regular expressions
    public static class RegexPatterns {
        // comments patterns
//        public Pattern JAVADOC_COMMENTS = Pattern.compile("/\\*\\*[^(\\*\\*/)]*?\\*/");
        public Pattern MULTILINE_COMMENTS = Pattern.compile("/\\*\\*?[^(\\*/)]*?\\*/");
        public Pattern LINE_END_COMMENTS = Pattern.compile("//.+?\\n");
        public Pattern LINE_COMMENTS = Pattern.compile("/\\*\\*?[^\\n(\\*/)]*?\\*/");
        
        // tabs and spaces patterns
        public Pattern LEADING_SPACES = Pattern.compile("(\\n\\s+)|(^\\s+)");
        public Pattern TRAILING_SPACES = Pattern.compile("(\\s+\\n)|(\\s+$)");
        
        // empty lines
        public Pattern EMPTY_LINES = Pattern.compile("[\\n]+");
        
        // value types
        public Pattern VALUE_BOOLEAN = Pattern.compile("\\s*(true|false)\\s*");
        public Pattern VALUE_INT = Pattern.compile("\\s*-?\\d+\\s*");
        public Pattern VALUE_DOUBLE = Pattern.compile("\\s*-?\\d+(\\.\\d+)?\\s*");
        public Pattern VALUE_STRING = Pattern.compile("\\s*\\\".+?\\\"\\s*");
        public Pattern VALUE_CHAR = Pattern.compile("\\s*'.'\\s*");
        
        // commands patterns
        public Pattern VARIABLE_DECLARATION = Pattern.compile(
                "((final)\\s+)?(int|double|boolean|char|String)\\s+(_[A-Za-z0-9_]+|[A-Za-z][A-Za-z0-9_]*)(\\s*=\\s*([^;]+))?;");
        public int VARIABLE_DECLARATION_GROUP_IS_FINAL = 2;
        public int VARIABLE_DECLARATION_GROUP_VAR_TYPE = 3;
        public int VARIABLE_DECLARATION_GROUP_NAME = 4;
        public int VARIABLE_DECLARATION_GROUP_VALUE = 6;

        public Pattern METHOD_DECLARATION = Pattern.compile(
                "(int|double|boolean|char|String|void)\\s+([A-Za-z][A-Za-z0-9_]*)\\s*\\((.*?)\\)\\s*\\{(.*)\\}",
                Pattern.DOTALL);
        public int METHOD_DECLARATION_GROUP_VAR_TYPE = 1;
        public int METHOD_DECLARATION_GROUP_NAME = 2;
        public int METHOD_DECLARATION_GROUP_ARGUMENTS_STRING = 3;
        public int METHOD_DECLARATION_GROUP_CODE = 4;

        public Pattern METHOD_ARGUMENTS = Pattern.compile(
                "\\s*((final)\\s+)?(int|double|boolean|char|String)\\s+(_|[A-Za-z][A-Za-z0-9_]*)\\s*");
        public int METHOD_ARGUMENTS_GROUP_IS_FINAL = 2;
        public int METHOD_ARGUMENTS_GROUP_VAR_TYPE = 3;
        public int METHOD_ARGUMENTS_GROUP_NAME = 4;

        public Pattern METHOD_CALL = Pattern.compile(
                "\\s*([A-Za-z][A-Za-z0-9_]*)\\s*\\(([^\\n]*)\\)\\s*;?\\s*");
        public int METHOD_CALL_GROUP_NAME = 1;
        public int METHOD_CALL_GROUP_ARGUMENTS_STRING = 2;

        public Pattern CONDITION_COMMAND = Pattern.compile(
                "(if|while)\\s*\\(([^{]+)\\)\\s*\\{(.*)\\}", Pattern.DOTALL);
        public int CONDITION_COMMAND_GROUP_CONDITION = 2;
        public int CONDITION_COMMAND_GROUP_CODE = 3;

        public Pattern RETURN_COMMAND = Pattern.compile(
                "\\s*(return)(\\s+(.+))?;");
        public int RETURN_COMMAND_GROUP_VALUE = 3;

        public Pattern ASSIGNMENT_COMMAND = Pattern.compile(
                "((_|[A-Za-z])[A-Za-z0-9_]*)\\s+=\\s+(.+);");
        public int ASSIGNMENT_COMMAND_GROUP_NAME = 1;
        public int ASSIGNMENT_COMMAND_GROUP_VALUE = 3;

        // argument types
        public Pattern ARGUMENT_VALUE = Pattern.compile("\\s*(true|false|-?\\d+|-?\\d+(\\.\\d+)?|\\\".+?\\\"|'.')\\s*");
        public Pattern ARGUMENT_VARIABLE = Pattern.compile("(_[A-Za-z0-9_]+|[A-Za-z][A-Za-z0-9_]*)");
        public Pattern ARGUMENT_CALL = METHOD_CALL;

    }

}

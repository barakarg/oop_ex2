package oop.ex2.main;

import static oop.ex2.main.ProgramConstants.RETURN_CODE_CORRECT;
import static oop.ex2.main.ProgramConstants.RETURN_CODE_INCORRECT;
import static oop.ex2.main.ProgramConstants.RETURN_CODE_IOERROR;

import java.io.IOException;

import oop.ex2.commands.CommandsBlock;
import oop.ex2.runtime.Environment;
import oop.ex2.runtime.Method;
import oop.ex2.validations.CodeBlockValidation;

public class Sjavac {

    private static String sourceFilePath;

    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {

        try {
            // init arguments
            initArguments(args);

            // init source file
            SourceCode sourceCode = new SourceCode(sourceFilePath);

            // parse the source code
            CommandsBlock globalCommandsBlock =
                    new CommandsBlock(sourceCode.getData());

            // initialize global environment
            Environment globalEnvironment = new Environment(globalCommandsBlock);

            // for each method in global environment
            for (Method method : globalEnvironment.getMethods()) {
                // check method validity
                CodeBlockValidation.validate(method, method.getCode(),
                        new Environment(globalEnvironment, method));
            }

            // exit with appropriate return code
            System.exit(RETURN_CODE_CORRECT);

        }
        catch (Exception e) {
            if (sourceFilePath.contains("Windows")) {
                throw e;
            } else {
                exceptionHandler(e);
            }
        }
    }


    private static void initArguments(String[] args) {
        // validate right number of arguments
        if (args.length != 1) {
            throw new IllegalArgumentException("wrong number of arguments.");
        }
        // assign arguments
        sourceFilePath = args[0];
    }


    private static void exceptionHandler(Exception e) throws Exception {
        // print exceptions description to stdout
        System.out.println(e.getMessage());

        // exit with appropriate return code
        System.exit(
                (e.getClass() == IOException.class)?
                        RETURN_CODE_IOERROR: RETURN_CODE_INCORRECT
                );

    }
}

package oop.ex2.main;

import static oop.ex2.main.ProgramConstants.NEWLINE_CHAR;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import oop.ex2.main.ProgramConstants.RegexPatterns;

/**
 * This class represents the source code that the program validates.
 *
 */
public class SourceCode {

    private String data;
    private RegexPatterns rp;

    /**
     * Create SourceFile instance and instantiate it.
     * @param path
     * @throws Exception 
     */
    public SourceCode(String path) throws Exception {
        // init regex patterns
        rp = new ProgramConstants.RegexPatterns();

        // read the file into 'data'
        data = "";
        importFromFile(path);

        // clean up file from comments and unneeded spaces
        cleanup();
    }

    /**
     * read file contents from a given path.
     * @param path source file path
     * @throws IOException
     */
    private void importFromFile(String path) throws IOException {
        // init streams variables
        FileReader fin = null;
        BufferedReader bfin = null;
        Scanner input = null;

        // open and read file
        try {
            //create Scanner object for source file
            fin = new FileReader(path);
            bfin = new BufferedReader(fin);
//            input = new Scanner(bfin);
            String character;

            while ((character = bfin.readLine()) != null) {
                data = data.concat(character).concat(NEWLINE_CHAR); // add new line char
            }
            //read lines from source file
//            while( input.hasNextLine() ) {
//                if (data.length()>0) { // (if not first line)
//                    data = data.concat(NEWLINE_CHAR); // add new line char
//                }
//                data = data.concat(input.nextLine());
//            }
        }
        finally {
            //close the streams
            if (fin != null) {
                if (bfin != null) {
//                    if (input != null) {
//                        input.close();
//                    }
                    bfin.close();
                }
                fin.close();
            }
        }
    }

    /**
     * clean the data from trailing spaces and comments.
     * @throws Exception 
     */
    private void cleanup() throws Exception {
        // remove all comments
        data = replaceAll(rp.LINE_COMMENTS, data, " ");
        data = replaceAll(rp.LINE_END_COMMENTS, data, NEWLINE_CHAR);
        data = replaceAll(rp.MULTILINE_COMMENTS, data, NEWLINE_CHAR);
//        data = replaceAll(rp.JAVADOC_COMMENTS, data, " ");

        // remove all tabs and trailing spaces
        data = replaceAll(rp.LEADING_SPACES, data, NEWLINE_CHAR);
        data = replaceAll(rp.TRAILING_SPACES, data, NEWLINE_CHAR);

        // remove empty lines
//        data = replaceAll(rp.EMPTY_LINES, data, NEWLINE_CHAR);
        
        // remove the last empty line
//        if (!data.endsWith(NEWLINE_CHAR)) {
//            throw new Exception("error - file must end with empty line.");
//        }
//        data.substring(0, -1);
    }

    /**
     * replace all instances of 'pattern' in 'input' with 'replaceWith'
     * @param pattern pattern to find
     * @param input input data
     * @param replaceWith text to replace the found expressions with
     * @return fixed data
     */
    private String replaceAll(Pattern pattern, String input, String replaceWith) {
        return pattern.matcher(input).replaceAll(replaceWith);
    }

    /**
     * return data
     * @return data
     */
    public String getData() {
        return data;
    }
}
